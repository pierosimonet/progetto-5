/*
 Semplice programma per arduino che legge un valore nella porta
 seriale ed lo ripropone in out.

 TODO:
 -simulare la matrice DONE
 -aggiungere il supporto per uno shermo DONE
 */

// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(13, 12, 11, 10, 9, 8);

byte comando[4] = {0,0,0,0};

//la matrice in uscita, setterò la cella come uscita
int matrice[4] = {0,0,0,0};

void printStatus(){
  lcd.clear();
  lcd.print("ENTRATA: 1 2 3 4");
  lcd.print("USCITA :");
  for(int i=0;i < 4;i++){
    String str=String(matrice[i]);
    lcd.print(" "+str);
    //Serial.print(matrice[i]);
  }
}

void setup(){
  Serial.begin(9600);
  lcd.begin(16, 2);
  // Print a message to the LCD.
  printStatus();
}


void loop(){
  if(Serial.available() >= 4) {

    comando[0]=Serial.read();
    comando[1]=Serial.read();
    comando[2]=Serial.read();
    comando[3]=Serial.read();

    if (comando[0] == 0x00){
      comando[0] = 0x40;
      //reset della matrice
      matrice[0] = 0;
      matrice[1] = 0;
      matrice[2] = 0;
      matrice[3] = 0;

    }else if(comando[0] == 0x01){

      //Definisco la posizione dell'imput ricavando dal secondo byte
      byte tmp = comando[1];
      byte tmpb = comando[2];
      tmp = tmp - 0x80;
      tmpb = tmpb - 0x80;
      matrice[(tmp-1)]=tmpb;

      comando[0] = 0x41 ;
    }else if(comando[0] == 0x05){
      //TODO controllare il numero salvato nell'impostazione
      comando[0] = 0x45 ;
      byte tmp = comando[2];
      tmp = tmp - 0x80;
      comando[2] = matrice[(tmp-1)] + 0x80;
    }
    Serial.write(comando[0]);
    Serial.write(comando[1]);
    Serial.write(comando[2]);
    Serial.write(comando[3]);
    //Serial.println();
    /*Serial.print(matrice[0]);
    Serial.print(matrice[1]);
    Serial.print(matrice[2]);
    Serial.print(matrice[3]);*/
    //Serial.println();
    printStatus();

  }
}
