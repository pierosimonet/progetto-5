#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
inte = "/home/piero/GitHub/Progetto-5/BackEnd/TUI/netfiles/interfaces"


def setip(ip, mask, gw):
    # Funzione che modifica il file /etc/network/interfaces inserendo l'ipv4
    # dato sottoforma di stringa (es "192.168.0.1") la sua netmask completa e
    # l'indirizzo di gateway corrispondente.
    # TODO controllare se i paramertri sono corretti

    stri = "iface eth0 inet static\n    adress "+ip+"\n   netmask "
    stri += "   gateway " + gw + "\n"

    f = open(inte"Main", 'r')
    os.remove("/etc/network/interfaces")
    f1 = open("/etc/network/interfaces", 'w')
    f1.write(f.read()+stri)
    f.close()
    f1.close()
    os.popen("reboot")
    return 0
