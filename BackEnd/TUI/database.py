#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sqlite3
import hashlib

PATH = "/home/piero/GitHub/Progetto-5/BackEnd/"
database = PATH + "Database/test.db"


def setDefoultRootPassword(password):
    # Fa ritornare la password di admin admin
    con = sqlite3.connect(database)
    a = "admin"
    # Utilizzo md5 per salvare le password
    tmp = hashlib.md5()
    tmp.update(a.encode())
    p = tmp.digest()
    con.execute("UPDATE UTENTI set PASSWORD=? where NAME=?;", (a, p))
    con.commit()
    return 0
