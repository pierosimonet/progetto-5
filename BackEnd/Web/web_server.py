#!/usr/bin/python3
# -*- coding: utf-8 -*-

import cherrypy
import os
from autenticazione import AuthController, require, member_of, name_is, g_u
import interface.comandi as c
import interface.database_interface as d
# TODO uniformare tutti i log e/o creare un metodo per essi
SERVERPORT = 8080
PATH = "/home/piero/GitHub/Progetto-5/BackEnd/Web/"


class RestrictedArea:
    # all methods in this controller (and subcontrollers) is
    # open only to members of the admin group

    _cp_config = {
        'auth.require': [member_of('admin')]
    }

    @cherrypy.expose
    def index(self):
        return """This is the admin only area."""


class Root:

    auth = AuthController()

    restricted = RestrictedArea()

    # This is only available if the user name is admin and he's in group admin
    @cherrypy.expose
    @require(name_is("admin"))
    @require(member_of("admin"))
    # equivalent: @require(name_is("admin"), member_of("admin"))
    def admin(self):
        return open("mat/admin.html")

    @cherrypy.expose
    @require()
    def index(self):
        return open(PATH + "index.html")
        # return """This page only requires a valid login."""

    @cherrypy.expose
    def open(self):
        return """This page is open to everyone"""

    @cherrypy.expose
    def logout(self):
        print("logout")
        self.auth.logout()


@cherrypy.expose
class sw_video(object):
    @cherrypy.tools.accept(media='text/plain')
    @cherrypy.tools.json_in()
    def GET(self, inte, to, mat):
        print("ricevuta richiesta")
        c.swich_video(int(inte), int(to), int(mat))


@cherrypy.expose
class get_video(object):
    @cherrypy.tools.accept(media='text/plain')
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def GET(self, num, inte, mat):
        print("richiesta video")
        return c.get_video_status(int(num), int(inte), int(mat))


@cherrypy.expose
class reset(object):
    @cherrypy.tools.accept(media='text/plain')
    def PUT(self, mat):
        print("ricevuta richiesta reset")
        c.reset_video(int(mat))


@cherrypy.expose
class get_conf(object):
    @cherrypy.tools.accept(media='text/plain')
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def GET(self):
        print("Ricevuta richiesta configurazioni salvate")
        return d.conf_to_xml()


@cherrypy.expose
class get_nome(object):
    @cherrypy.tools.accept(media='text/plain')
    def GET(self):
        return str(g_u()).upper()


@cherrypy.expose
class save_config(object):
    @cherrypy.tools.accept(media='text/plain')
    @cherrypy.tools.json_in()
    # TODO mettere l'input dinamico
    def GET(self, string, i1, i2, i3, i4):
        print("ricevuta richiesta salvataggio configurazione")
        tmp = []
        tmp.append(int(i1))
        tmp.append(int(i2))
        tmp.append(int(i3))
        tmp.append(int(i4))
        d.add_conf(string, tmp)
        return "0"


@cherrypy.expose
class rm_config(object):
    @cherrypy.tools.accept(media='text/plain')
    @cherrypy.tools.json_in()
    def GET(self, i):
        print("ricevuta richiesta rimozione configurazione")
        d.rm_conf(i)
        return "0"


@cherrypy.expose
class apply_config(object):
    @cherrypy.tools.accept(media='text/plain')
    @cherrypy.tools.json_in()
    def GET(self, i, mat):
        print("ricevuta richiesta applicazione configurazione")
        vet = d.get_vet_from(str(i))
        c.swich_video_complete(vet, int(mat))
        return "0"


def start():
    # viene creato il server sulla porta 80
    # TODO settare un protocollo ssl qui
    _tmp = {
        'server.socket_host': '0.0.0.0',
        'server.socket_port': SERVERPORT
    }
    _cp_config = {
        '/':  {
            'tools.auth.on': True,
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd())
        },
        '/sw_video': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'text/plain')],
        },
        '/reset': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'text/plain')],
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': PATH + 'public'
        },
        '/get_video': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'text/plain')],
        },
        '/mat': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': PATH + 'mat'
        },
        '/get_conf': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'text/plain')],
        },
        '/get_nome': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'text/plain')],
        },
        '/rm_config': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'text/plain')],
        },
        '/save_config': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'text/plain')],
        },
        '/apply_config': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'text/plain')],
        }
    }

    webapp = Root()
    webapp.sw_video = sw_video()
    webapp.get_video = get_video()
    webapp.reset = reset()
    webapp.get_conf = get_conf()
    webapp.get_nome = get_nome()
    webapp.rm_config = rm_config()
    webapp.save_config = save_config()
    webapp.apply_config = apply_config()
    cherrypy.config.update(_tmp)
    cherrypy.quickstart(webapp, '/', _cp_config)


if __name__ == '__main__':
    start()
