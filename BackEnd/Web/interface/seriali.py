import serial
import glob
import os
index = "/home/piero/GitHub/Progetto-5/BackEnd/Web/index.html"
mat = "/home/piero/GitHub/Progetto-5/BackEnd/Web/mat/mat"

# TODO identificare la matrice con il suo numero impostato

###############################################################################
# Controllo la presenza di matrici ed ne inizializzo il vettore
temp_list = glob.glob('/dev/tty[A-Za-z]*')
result = []

for a_port in temp_list:
    try:
        # TODO verificare la descrizione della porta sia corretta
        s = serial.Serial(a_port)
        s.close()
        result.append(a_port)
    except serial.SerialException:
        pass

print("[Sono state trovate "+str(len(result))+" matrici]")
print(result)

ser = []

for i in range(len(result)):
    ser.append(serial.Serial("/dev/ttyUSB0",  # result[0],
                             9600,
                             serial.EIGHTBITS,
                             serial.PARITY_NONE,
                             serial.STOPBITS_ONE,
                             timeout=0.1))
###############################################################################
# Rendo esistenti i vari file presetni in /mat per poter essere ricevuti
# dall'index a seconda della lungezza del vettore di partenza
try:
    os.remove(mat+"1.html")
    pass
except Exception as e:
    print("[logging]: file mat1 non precedentemente presente")
try:
    os.remove(mat+"2.html")
    pass
except Exception as e:
    print("[logging]: file mat2 non precedentemente presente")
try:
    os.remove(mat+"3.html")
    pass
except Exception as e:
    print("[logging]: file mat3 non precedentemente presente")
try:
    os.remove(mat+"4.html")
    pass
except Exception as e:
    print("[logging]: file mat4 non precedentemente presente")


def cp(stri):
    f = open(mat+stri+".tmp", 'r')
    f1 = open(mat+stri, 'w')
    f1.write(f.read())
    f.close()
    f1.close()

if(len(result) > 0):
    cp("1.html")
    if(len(result) > 1):
        cp("2.html")
        if(len(result) > 2):
            cp("3.html")
            if(len(result) > 3):
                cp("4.html")

###############################################################################


def immetti(comando, i):
    # Funzione che immette un comando nella porta seriale definito come
    # un array di 4 byte e ne verifica ed inoltra la risposta
    # Ed l'indice della matrice partendo da 0, 1, 2
    if (int(i) > (len(ser)-1)):
        return -3
    flushIn(i)
    if(len(comando) != 4):
        return -1   # Lughezza errata
    ser[i].write(comando)
    risposta = []
    for j in range(4):
        risposta.append(ser[i].readline(1))
    return risposta


def flushIn(i):
    # Funzione che pulisce il buffer i lettura della seriale
    ser[i].flushInput()
    ser[i].flushOutput()
