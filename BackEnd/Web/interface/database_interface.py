import sqlite3 as sql
PATH = "/home/piero/GitHub/Progetto-5/BackEnd/"
database = PATH + "Database/test.db"
# TODO standardizzare il codice con i ? e non con varibili vaire
# TODO aggiungere i vari controlli e controllare le funzioni
# TODO aggiungere una autogestione degli indici
# TODO controllare passagio di int o stringhe
# TODO Aggiungere i salvataggi per le varie matrici a sestanti
# ------------------------------------------------------------------------------
# GESTIONE UTENTI


def update_password(u, p):
    # Aggiorna la password @p all'utente @u
    if (controlla_utente(u) != 0):
        return -1
    con = sql.connect(database)
    con.execute("UPDATE UTENTI set PASSWORD=? where NAME=?;", (u, p))
    con.commit()
    con.close()
    return 0


def add_utente(u, p):
    # Aggiunge l'utente @u con la password @p
    if (controlla_utente(u) != 0):
        return -1
    con = sql.connect(database)
    con.execute("INSERT INTO UTENTI (NAME,PASSWORD) VALUES (?,?);", (u, p))
    con.commit()
    con.close()
    return 0


def rm_utente(u):
    # Rimuove l'utente desiderato basta non sia admin
    # TODO verificare con la password dell'utente stesso
    if (controlla_utente(u) != 0):
        return -1
    if (u == "admin"):
        return -2
    con = sql.connect(database)
    con.execute("DELETE FROM UTENTI WHERE NAME = \""+u+"\";")
    con.commit()
    con.close()
    return 0


def login(u, p):
    # convalida la password @p per l'utente @u
    tmp = -1
    if (controlla_utente(u) != -1):
        return -1
    con = sql.connect(database)
    # Per ricevere direttamnete la stringa uso Row
    con.row_factory = sql.Row
    # Creo un cursore
    c = con.cursor()
    c = con.execute("SELECT PASSWORD FROM UTENTI WHERE NAME = \""+u+"\"")
    r = c.fetchone()
    # TODO controllare se la lunghezza è piu lunda di 1
    # Controllo le password
    if (p == r[0]):
        tmp = 0
    con.close()
    return tmp


def controlla_utente(u):
    # Controlla la presenza di un dato utente @u nel database
    tmp = 0
    con = sql.connect(database)
    con.row_factory = sql.Row
    c = con.cursor()
    c.execute("SELECT NAME FROM UTENTI WHERE NAME = \""+u+"\"")
    r = c.fetchone()
    if r is not None:
        if (u == r[0]):
            tmp = -1
    con.close()
    return tmp
# ------------------------------------------------------------------------------
# GESTIONE CONFIGURAZIONI


def add_conf(name, vet):
    # Salva la configurazione con dati parametri
    # TODO controllo la presenza nel database della configurazione in i
    # TODO eventuale shift in basso del resto del database
    st = vet_to_str(vet)
    con = sql.connect(database)
    con.row_factory = sql.Row
    c = con.cursor()
    # Prelevo il numero delle configurazioni
    c.execute("SELECT N FROM N_C WHERE ID = 1")
    r = c.fetchone()
    i = int(r[0])
    i += 1
    con.execute("UPDATE N_C SET N = ?  WHERE ID = 1;", (str(i)))
    con.commit()
    con.execute("INSERT INTO CONFIGURAZIONI VALUES (?,?,?);", (i, name, st))
    con.commit()
    con.close()
    return 0


def rm_conf(i):
    # Rimuove la configurazione salvata con @i
    if (i == 0):
        # Verifico se devo eliminare o meno
        # TODO controllare se stringa o numero troppo grande
        return -2
    con = sql.connect(database)
    con.row_factory = sql.Row
    c = con.cursor()
    # Prelevo il numero delle configurazioni
    c.execute("SELECT N FROM N_C WHERE ID = 1")
    r = c.fetchone()
    j = int(r[0])
    if (j == 0):
        # Non ci sono configurazioni, giusto in caso serva verificarlo
        con.close()
        return -1
    tmp = j - 1
    con.execute("UPDATE N_C SET N = ?  WHERE ID = 1;", (str(tmp)))
    con.commit()
    con.execute("DELETE FROM CONFIGURAZIONI WHERE ID = ?;", i)
    con.commit()
    # Slitto le altre vonfigurazioni
    if (int(i) < j):
        for k in range(int(i), j+2):
            # print("i=? j=?", (k, j))
            swich_id(k)
    con.close()
    return 0


def get_vet_from(i):
    # Ritorna la configurazione salvata in @i
    con = sql.connect(database)
    con.row_factory = sql.Row
    c = con.cursor()
    c.execute("SELECT VECTOR FROM CONFIGURAZIONI WHERE ID = ?", (i))
    r = c.fetchone()
    # TODO Controllare che sia presete un solo valore
    tmp = str_to_vet(r[0])
    con.close()
    return tmp


def swich_id(i):
    # Sposta nella posizione inferiore la configurazione salvata in @i
    # TODO agiornare la posizione in qualsivoglia numero
    con = sql.connect(database)
    con.execute("UPDATE CONFIGURAZIONI set ID=? where ID=?;", ((i-1), i))
    con.commit()
    con.close()
    return 0


def swich_to_id(i, j):
    # Sposta nella posizione @j la configurazione salvata in @i
    # TODO agiornare la posizione in qualsivoglia numero
    con = sql.connect(database)
    con.execute("UPDATE CONFIGURAZIONI set ID=? where ID=?;", (j, i))
    con.commit()
    con.close()
    return 0


def vet_to_str(v):
    # Trasforma il vettore in una stringa da salvare nel database
    # TODO controllare la grandezza del vettore
    # tmp = "[" # Evito dato lo split sulla virgola
    tmp = ""
    for i in range(len(v)-1):
        tmp += str(v[i])
        tmp += ','
    i += 1
    tmp += str(v[i])
    # tmp += "]"
    return tmp


def str_to_vet(s):
    # Funzione che ricrea la configurazione data la stringa salvata nel db
    tmp = []
    # TODO implementarlo con sisitemi di regular expression
    ts = s.split(",")
    for i in range(len(ts)):
        tmp.append(int(ts[i]))
    return tmp
# ------------------------------------------------------------------------------
# Funzione che ritorna le varie configurazione salvate in una unica stringa xml


def conf_to_xml():
    st = ""
    con = sql.connect(database)
    con.row_factory = sql.Row
    c = con.cursor()
    c.execute("SELECT NAME FROM CONFIGURAZIONI ")
    i = 1
    while True:
        r = c.fetchone()
        if r is None:
            break
        if (i == 1):
            st += "<table class='conf'><tr><td>Num</td><td>Nome</td>"
            st += "<td>U1</td><td>U2</td><td>U3</td><td>U4</td><td></td></tr>"
        st += "<tr><td><id>"+str(i)+"</id></td>"
        st += "<td><name>"+r[0]+"</name></td>"
        v = get_vet_from(str(i))
        for j in range(len(v)):
            t = str(j+1)
            st += "<td><u"
            st += t
            st += ">"
            st += str(v[j])
            st += "</u"
            st += t
            st += "></td>"
        st += "<td>"
        st += """<button onClick="$.ajax({type: 'GET',url: '/apply_config',
            data: {'i': """
        st += str(i)
        st += """, 'mat': 0} }).done(function() {geta();get_conf();});">Applica</button>"""
        st += "</td>"
        st += "</tr>"
        i += 1
    st += "</tabel>"
    con.close()
    return st
# ------------------------------------------------------------------------------
