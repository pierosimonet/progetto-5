import interface.seriali as s
# Questo file conterrà i vari comandi che il server manderà alla matrice
# I comandi saranno del tipo comando(in,out) ed verranno direttamente seguiti
# Inizialmente solo video, per l'audio in futuro basterà cambiare il byte di
# riferimento come da documentazione
# TODO controllare tutta la risposta
max_matrice = 4
num_matrice = 0x81


def reset_video(mat):
    # Resetta tutti gli output della matrice
    risp = s.immetti([0x00, 0x81, 0x81, num_matrice], int(mat))
    tp = int.from_bytes(risp[0], byteorder='big')
    tp -= 0x40
    if (tp != 0x0):
        return -2
    else:
        return 0


def swich_video(int, out, mat):
    # Immette nel canale @inp l'uscita @out, ne controlla poi l'effettivo
    # cambiamento controllando lo stato di quell'uscita
    if (inp < 1 or inp > max_matrice or out < 0 or out > max_matrice):
        return -1
    tinp = inp + 0x80  # Completo con il bit per il protocollo
    tout = out + 0x80
    risp = s.immetti([0x01, tinp, tout, num_matrice], int(mat))
    tp = int.from_bytes(risp[0], byteorder='big')
    tp -= 0x40
    if (tp != 0x1):
        # Controllo se la risposta sia indirizzata a me
        return -2
    tmp = get_video_status(0, inp, mat)
    if (int != tmp):
        return -3
    return 0


def store_video(inp, cond, mat):
    # Salva o rimuover la configurazione attuale in memoria in posizione @inp
    # tramite la codizione @cond 0 -> salva 1 -> elimina
    if (cond != 1 or cond != 0):
        return -1
    inp += 0x80
    cond += 0x80
    risp = s.immetti([0x03, inp, cond, num_matrice], int(mat))
    tp = int.from_bytes(risp[0], byteorder='big')
    tp -= 0x40
    if (tp != 0x3):
        return -2
    else:
        return 0


def recall_video(num, mat):
    # Richiama la configurazione salvata in @inp
    if (num < 0):
        # Errore di input
        return -1
    num += 0x80
    risp = s.immetti([0x04, num, 0x80, num_matrice], int(mat))
    tp = int.from_bytes(risp[0], byteorder='big')
    tp -= 0x40
    if (tp != 0x04):
        return -2
    else:
        return 0


def get_video_status(num, inp, mat):
    # Richima nella configurazione @num (0 per l'attuale)
    # l'uscita settata in @inp
    # if (inp < 0 or inp > max_matrice or num < 0):
    #    Errore di input
    #    return -1
    num += 0x80
    inp += 0x80
    risp = s.immetti([0x05, num, inp, num_matrice], int(mat))
    tmp = int.from_bytes(risp[0], byteorder='big')
    tmp -= 0x40
    if (tmp != 0x05):
        # Verifico se si sono effetuati errori
        return -2
    tmp = int.from_bytes(risp[2], byteorder='big')
    tmp -= 0x80
    return tmp


def get_video_complete_status(num, mat):
    # Restituisce un vettore lungo @max_matrice contenente la configurazione
    # salvata in @num (0 per l'attuale)
    if (num < 0):
        # Errore di input
        return -1
    vet = []
    for i in range(1, (max_matrice + 1)):
        tmp = get_video_status(num, i, mat)
        if (tmp < 0):
            # Se si sono verificati errori li riporto
            return tmp
        vet.append(tmp)
    return vet


def swich_video_complete(vet, mat):
    # Immette nella matrice la configurazione mandata per parametro e ne
    # controlla l'effettivo inserimento
    if (len(vet) != max_matrice):
        # Grandezza matrice errata
        return -4
    for i in range(1, (max_matrice+1)):
        tmp = i - 1
        tmp = swich_video(i, vet[tmp], mat)
        if (tmp < 0):
            return tmp
    tmp = get_video_complete_status(0, mat)
    for i in range(memoryview):
        if(vet[i] != tmp[i]):
            # Si sono erificati errori nell'input
            return -5
    return 0
