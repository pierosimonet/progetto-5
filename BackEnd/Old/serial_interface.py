import serial

ser = serial.Serial('/dev/ttyACM0',         # TODO autoricerca della porta
                    115200,                 # 9600
                    serial.EIGHTBITS,       # l'unita base è di un byte
                    serial.PARITY_NONE,     # non uso il controllo di parità
                    serial.STOPBITS_ONE,    # definisco la chiusura dei bit
                    timeout=None)           # non uso un timeout per la lettura


def immetti(comando):
    # Funzione che immette un comando nella porta seriale definito come
    # un array di 4 byte e ne verifica ed inoltra la risposta
    flushIn()
    if(len(comando) != 4):
        return -1   # Lughezza errata
    ser.write(comando)
    risposta = []
    for i in range(4):
        risposta.append(ser.readline(1))
    return risposta


def flushIn():
    # Funzione che pulisce il buffer i lettura della seriale
    ser.flushInput()
    ser.flushOutput()
