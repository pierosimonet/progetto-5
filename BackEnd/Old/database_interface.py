import sqlite3 as sql
database = "../Database/test.db"
# TODO standardizzare il codice con i ? e non con varibili vaire
# TODO aggiungere i vari controlli e controllare le funzioni
# TODO aggiungere una autogestione degli indici
# TODO controllare passagio di int o stringhe
# ------------------------------------------------------------------------------
# GESTIONE UTENTI


def update_password(u, p):
    # Aggiorna la password @p all'utente @u
    if (controlla_utente(u) != 0):
        return -1
    con = sql.connect(database)
    con.execute("UPDATE UTENTI set PASSWORD=? where NAME=?;", (u, p))
    con.commit()
    con.close()
    return 0


def add_utente(u, p):
    # Aggiunge l'utente @u con la password @p
    if (controlla_utente(u) != 0):
        return -1
    con = sql.connect(database)
    con.execute("INSERT INTO UTENTI (NAME,PASSWORD) VALUES (?,?);", (u, p))
    con.commit()
    con.close()
    return 0


def rm_utente(u):
    # Rimuove l'utente desiderato basta non sia admin
    # TODO verificare con la password dell'utente stesso
    if (controlla_utente(u) != 0):
        return -1
    if (u == "admin"):
        return -2
    con = sql.connect(database)
    con.execute("DELETE FROM UTENTI WHERE NAME = \""+u+"\";")
    con.commit()
    con.close()
    return 0


def login(u, p):
    # convalida la password @p per l'utente @u
    tmp = -1
    if (controlla_utente(u) != 0):
        return -1
    con = sql.connect(database)
    # Per ricevere direttamnete la stringa uso Row
    con.row_factory = sql.Row
    # Creo un cursore
    c = con.cursor()
    c = con.execute("SELECT PASSWORD FROM UTENTI WHERE NAME = \""+u+"\"")
    r = c.fetchone()
    # TODO controllare se la lunghezza è piu lunda di 1
    # Controllo le password
    if (p == r[0]):
        tmp = 0
    con.close()
    return tmp


def controlla_utente(u):
    # Controlla la presenza di un dato utente @u nel database
    tmp = 0
    con = sql.connect(database)
    con.row_factory = sql.Row
    c = con.cursor()
    c.execute("SELECT NAME FROM UTENTI WHERE NAME = \""+u+"\"")
    r = c.fetchone()
    # TODO fare un solo controllo e non un ciclo inutile
    for i in range(len(r)):
        if (u == r[i]):
            tmp = -1
    con.close()
    return tmp
# ------------------------------------------------------------------------------
# GESTIONE CONFIGURAZIONI


def add_conf(i, name, vet):
    # Salva la configurazione con dati parametri
    # TODO controllo la presenza nel database della configurazione in i
    # TODO eventuale shift in basso del resto del database
    st = vet_to_str(vet)
    con = sql.connect(database)
    con.execute("INSERT INTO CONFIGURAZIONI VALUES (?,?,?);", (i, name, st))
    con.commit()
    con.close()
    return 0


def rm_conf(i):
    # Rimuove la configurazione salvata con @i
    con = sql.connect(database)
    con.execute("DELETE FROM CONFIGURAZIONI WHERE ID = ?;", i)
    con.commit()
    con.close()
    return 0


def get_vet_from(i):
    # Ritorna la configurazione salvata in @i
    con = sql.connect(database)
    con.row_factory = sql.Row
    c = con.cursor()
    c.execute("SELECT VECTOR FROM CONFIGURAZIONI WHERE ID = ?", (i))
    r = c.fetchone()
    # TODO Controllare che sia presete un solo valore
    tmp = str_to_vet(r[0])
    con.close()
    return tmp


def swich_id(i):
    # Sposta nella posizione superiore la configurazione salvata in @i
    # TODO agiornare la posizione in qualsivoglia numero
    con = sql.connect(database)
    con.execute("UPDATE CONFIGURAZIONI set ID=? where ID=?;", ((i+1), i))
    con.commit()
    con.close()
    return 0


def swich_to_id(i, j):
        # Sposta nella posizione superiore la configurazione salvata in @i
        # TODO agiornare la posizione in qualsivoglia numero
        con = sql.connect(database)
        con.execute("UPDATE CONFIGURAZIONI set ID=? where ID=?;", (j, i))
        con.commit()
        con.close()
        return 0


def vet_to_str(v):
    # Trasforma il vettore in una stringa da salvare nel database
    # TODO controllare la grandezza del vettore
    # tmp = "[" # Evito dato lo split sulla virgola
    tmp = ""
    for i in range(len(v)-1):
        tmp += str(v[i])
        tmp += ','
    i += 1
    tmp += str(v[i])
    # tmp += "]"
    return tmp


def str_to_vet(s):
    # Funzione che ricrea la configurazione data la stringa salvata nel db
    tmp = []
    # TODO implementarlo con sisitemi di regular expression
    ts = s.split(",")
    for i in range(len(ts)):
        tmp.append(int(ts[i]))
    return tmp
# ------------------------------------------------------------------------------
