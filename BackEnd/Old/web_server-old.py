import cherrypy
import socket
import os
import os.path

HOST, PORT = "localhost", 8989
server = ''


class ConnectToBackend():
    sock = ''

    def inizialize(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Connect to server and send data
        self.sock.connect((HOST, PORT))
        initial = str(self.sock.recv(1024), "utf-8")
        if initial is None:
            return -1
        else:
            return 0

    def send(self, data):
        self.sock.sendall(bytes(data + "\n", "utf-8"))
        return str(self.sock.recv(1024), "utf-8")
    pass


class Page(object):
    @cherrypy.expose
    def index(self):
        return open('index.html')


@cherrypy.expose
class WebService(object):
    @cherrypy.tools.accept(media='text/plain')
    def PUT(self, inte, to):
        server.send("swich_video(" + inte + "," + to + ")")

    def POST(self):
        server.send("reset")

if __name__ == '__main__':
    server = ConnectToBackend()
    server.inizialize()

    conf = {
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd())
        },
        '/comando': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'text/plain')],
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './public'
        }
    }

    webapp = Page()
    webapp.comando = WebService()
    cherrypy.quickstart(webapp, '/', conf)
