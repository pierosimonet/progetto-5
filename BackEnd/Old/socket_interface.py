import socketserver
import regular_expression as re
import comandi

HOST = "localhost"
PORT = 8989

WELCOME_MESSAGE = "SERVER GESTORE MATRICE"
ERROR_MESSAGE = "COMANDO NON IDENTIFICATO"
INFO_MESSAGE = "Scivere:\n-reset per resettare la matrice\n-info per le informazioni ai vari comandi\n-swich_video(A,B) per sposare l'uscita B con l'entrata A"

# scoket TCP per tutte le comunicazioni
# TODO ricreare la stessa classe su UNIXSOCKET


class MyTCPHandler(socketserver.StreamRequestHandler):
    def codifica_comando(self, data):
        if (data == bytes("info", "utf-8")):
            return bytes(INFO_MESSAGE + "\n", "utf-8")
        elif (data == bytes("reset", "utf-8")):
            comandi.reset_video()
            return bytes("OK"+"\n", "utf-8")
        elif (re.swich(data.decode("utf-8")) >= 0):
            return bytes("OK"+"\n", "utf-8")
        else:
            return bytes(ERROR_MESSAGE + "\n", "utf-8")

    def handle(self):
        self.wfile.write(bytes(WELCOME_MESSAGE+"\n", "utf-8"))
        # TODO aggionare e controllare se la connessione resta e non 1
        while (True):
            # self.rfile is a file-like object created by the handler;
            # we can now use e.g. readline() instead of raw recv() calls
            self.data = self.rfile.readline().strip()
            print("{} wrote:".format(self.client_address[0]))
            print(self.data)
            self.wfile.write(self.codifica_comando(self.data))

    pass


def start_server():
    server = socketserver.TCPServer((HOST, PORT), MyTCPHandler)
    server.serve_forever()

# Per il testing della clsse
if __name__ == "__main__":
    start_server()
