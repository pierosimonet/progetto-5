#BackEND
<p>Applicazione lato server per il governo della matrice, principalmente scritto in python per la sua modularità e semplicità di scrittura oltre alla gestione automatica della memoria</p>
<h2>Dipendenze</h2>
<ul>
  <li>Python3</li>
  <li>cherrypy</li>
  <li>pyserial</li>
</ul>
<h2>TODO</h2>
<ul>
  <li>Gestione errori</li>
  <li>Migliorare la agina web per la sua utilizzo</li>
  <li>File di configurazione</li>
  <li>Gestione utenti (creazione, rimozione)</li>
  <li>Codici xml migliorato</li>
  <li>Salvataggi per piu matrici nel db</li>
</ul>
<h2>BUGS</h2>
<ul>
  <li>Gestione multimatrici</li>
  <li>Gestione responsive layout</li>
  <li>db applia solo alla prima matrice</li>
  <li>La visualizzazione delle imostazioni impazzische</li>
  <li>Va in loop su rasberrypi</li>
</ul>
