import sqlite3
import hashlib
# Questo file inizializzerà i database com le varie tabelle necessarie
con = sqlite3.connect("test.db")

print("Connesione effetuata")
print("Creazione tabella configurazioni...")
con.execute("DROP TABLE IF EXISTS CONFIGURAZIONI;")
con.commit()
con.execute('''CREATE TABLE CONFIGURAZIONI
            ( ID            INTEGER PRIMARY KEY     AUTOINCREMENT,
              NAME          TEXT                    NOT NULL,
              VECTOR        TEXT                    NOT NULL
            );''')

con.execute("DROP TABLE IF EXISTS N_C;")
con.commit()
con.execute('''CREATE TABLE N_C
            ( ID            INTEGER PRIMARY KEY     NOT NULL,
              N             INT                    NOT NULL
            );''')
con.execute("INSERT INTO N_C VALUES (1,0);")
con.commit()

print("Creazione tabella utenti...")
# TODO aggiungere i permessi
con.execute("DROP TABLE IF EXISTS UTENTI;")
con.commit()
con.execute('''CREATE TABLE UTENTI
            ( ID            INTEGER PRIMARY KEY     AUTOINCREMENT,
              NAME          TEXT                    NOT NULL,
              PASSWORD      TEXT                    NOT NULL
            );''')

print("Creazione utente admin...")
a = "admin"
# Utilizzo md5 per salvare le password
tmp = hashlib.md5()
tmp.update(a.encode())
p = tmp.digest()
con.execute("INSERT INTO UTENTI (NAME,PASSWORD) VALUES (?,?);", (a, p))
con.commit()

con.close()

print("Procedimento completato")
