/* 4Byte
 *
 * 0  D  N5 N4 N3 N2 N1 N0
 * 7  6  5  4  3  2  1  0
 *
 * 7 -> 0 defaunt
 * 6 -> 0 to swich / 1 to pc
 * N5...N0 -> INSTRUCTION
 *
 * 1 I6 I5 I4 I3 I2 I1 I0
 * 7 6  5  4  3  2  1  0
 *
 * 7 -> 1 defaunt
 * I6...I0 -> imput
 *
 * 1 O6 O5 O4 O3 O2 O1 O0
 * 7 6  5  4  3  2  1  0
 *
 * 7 -> 1 defaunt
 * O6...O0 -> output
 *
 * 1 OVR X M4 M3 M2 M1 M0
 * 7 6   5 4  3  2  1  0
 *
 * 7 -> 1 defaunt
 * OVR -> Broadcast del comando
 * X -> inifluente
 * M4...M0 -> Numero della matrice // se utilizzo una matrice sola in seriale metto questi bit a 1 e la matrice settata a 1
 *
 */
