import socket
import sys

HOST, PORT = "localhost", 8989
data = "".join(sys.argv[1:])

# Create a socket (SOCK_STREAM means a TCP socket)
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    # Connect to server and send data
    sock.connect((HOST, PORT))
    initial = str(sock.recv(1024), "utf-8")
    sock.sendall(bytes(data + "\n", "utf-8"))

    # Receive data from the server and shut down
    received = str(sock.recv(1024), "utf-8")


initial = initial.strip('\n')
print("Received: {}".format(initial))
print("Sent:     {}".format(data))
print("Received: {}".format(received))
